import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    // return this.appService.getHello();
    return 'Default';
  }

  @Get('hello')
  getHello(): string {
    // return this.appService.getHello();
    return '<html><body><h1>Hello Buu!</h1></body></html>';
  }

  @Delete('world')
  getWorld(): string {
    // return this.appService.getHello();
    return '<html><body><h1>World Buu!</h1></body></html>';
  }

  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    // return this.appService.getHello();
    return { celsius: celsius, type: type };
  }

  @Get('test-params/:celsius')
  testParam(@Req() req, @Param('celsius') celsius: number) {
    // return this.appService.getHello();
    return { celsius };
  }

  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    // return this.appService.getHello();
    return { celsius };
  }
}
