import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    // return {
    //   celsius: parseFloat(celsius),
    //   fahrenheit: (parseFloat(celsius) * 9.0) / 5 + 32,
    // };
    return this.temperatureService.convert(parseFloat(celsius));
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    // return {
    //   celsius: celsius,
    //   fahrenheit: (celsius * 9.0) / 5 + 32,
    // };

    return this.temperatureService.convert(celsius);
  }

  @Get('convert/:celsius')
  convertByParams(@Param('celsius') celsius: string) {
    // return {
    //   celsius: parseFloat(celsius),
    //   fahrenheit: (parseFloat(celsius) * 9.0) / 5 + 32,
    // };
    return this.temperatureService.convert(parseFloat(celsius));
  }
}
